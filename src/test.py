import sys
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtGui import QColor
from PyQt5 import QtGui, QtCore
from functools import partial
from user import User
from addUserDialog import AddUserDlg, EditUserDlg, CreatUserDlg
from getDriver import Chrome
from uiLogin import login




class TabDemo(QTabWidget):
    def __init__(self, parent=None):
        super(TabDemo, self).__init__(parent)
        self.tab1 = QWidget()
        self.tab2 = QWidget()
        self.addTab(self.tab1, "测试")
        self.addTab(self.tab2, "预发")
        self.tab1UI()
        self.tab2UI()
        self.setWindowTitle("FastLogin")
        self.resize(400, 600)

    def tab1UI(self):
        self.initUI(0)
        print("test")

    def tab2UI(self):
        self.initUI(1)
        print("staging")

    def initUI(self, env):
        # 创建布局
        self.guinie_h_layout = QHBoxLayout()
        self.User_h_layout = QHBoxLayout()
        self.top_v_layout = QVBoxLayout()
        self.users_v_layout = QVBoxLayout()
        self.bottom_v_layout = QHBoxLayout()
        self.all_v_layout = QVBoxLayout()

        # palette = QtGui.QPalette()
        # palette.setColor(self.backgroundRole(), QColor(45, 58, 75))
        # self.setPalette(palette)
        # self.setAutoFillBackground(True)
        # self.setWindowOpacity(0.95)

        # 添加用户按钮
        self.button_add = QPushButton('添加测试用户', self)
        self.button_add.setToolTip("添加测试用户")
        self.button_add.setMinimumHeight(30)
        # self.setButtonStyle(self.button_add)
        self.button_add.clicked.connect(self.addUserDlg)

        # 创建用户按钮
        self.button_creat = QPushButton('创建测试用户', self)
        self.button_creat.setToolTip("创建测试用户")
        self.button_creat.setMinimumHeight(30)
        # self.setButtonStyle(self.button_creat)
        self.button_creat.clicked.connect(self.creatUserDlg)

        # 圭臬灰度输入框
        self.guinie_lable = QLabel('隔离组cookie:', self)
        self.guinie_line = QLineEdit('guinie-isolation-63', self)
        self.guinie_line.setMinimumHeight(36)
        self.guinie_line.setAttribute(QtCore.Qt.WA_MacShowFocusRect, 0)
        # self.guinie_lable.setStyleSheet("QLabel{color:rgb(191, 203, 217)}")
        # self.setLineEditStyle(self.guinie_line)

        # 创建/新增用户并排布局


        # top顶部布局
        # self.top_v_layout.addWidget(self.button_add)
        # self.top_v_layout.addWidget(self.button_creat)
        self.User_h_layout.addWidget(self.button_creat)
        self.User_h_layout.addWidget(self.button_add)
        self.top_v_layout.addLayout(self.User_h_layout)
        self.guinie_h_layout.addWidget(self.guinie_lable)
        self.guinie_h_layout.addWidget(self.guinie_line)
        self.top_v_layout.addLayout(self.guinie_h_layout)

        # 加载测试用户列表
        self.initUsers(env)
        # self.users_v_layout.addStretch(1)

        # 布全局局
        self.all_v_layout.addLayout(self.top_v_layout)
        self.all_v_layout.addLayout(self.users_v_layout)
        self.all_v_layout.setContentsMargins(15, 10, 15, 10)
        self.setLayout(self.all_v_layout)

# 加载users
    def initUsers(self, env):
        # 遍历展示测试账户按钮
        # 维护一个本地文件去获取测试账户
        user = User()
        users = user.get_user(env)
        for index, user in enumerate(users):
            button_username = QPushButton(user['type'] + ':' + user['username'], self)
            button_username.setToolTip("去快速登陆政采云")
            button_username.setMinimumHeight(36)
            # 设置按钮样式
            # self.setUsertyle(button_username)
            button_username.setMinimumWidth(180)
            # # 账户角色type
            # user_label = QLabel(user['type'], self)
            # user_label.setStyleSheet(
            #     "QLabel{color:rgb(191, 203, 217)}"
            # )

            # 查看/编辑/删除
            button_edit = QPushButton('编辑', self)
            button_del = QPushButton('删除', self)

            # self.setButtonStyle(button_edit)
            # self.setButtonStyle(button_del)

            # 单个用户操作横向布局
            user_h_layout = QHBoxLayout()
            # user_h_layout.addWidget(user_label)
            user_h_layout.addWidget(button_username)
            user_h_layout.addWidget(button_edit)
            user_h_layout.addWidget(button_del)

            self.users_v_layout.addLayout(user_h_layout)

            # lambda 函数传递变量地址（无效）
            # button_username.clicked.connect(lambda: self.on_click(user['username'], user['password']))
            # partial 偏函数实时传递值（花了一天时间，记录一下）
            button_username.clicked.connect(partial(self.on_click, user['env'],
                                                    user['username'], user['password'], user['preset_url']))
            button_edit.clicked.connect(partial(self.editUser, user))
            button_del.clicked.connect(partial(self.delUser, user))

    # 添加用户弹窗
    def addUserDlg(self):
        dialog = AddUserDlg(self)
        if dialog.exec_():  # accept 则返回1，reject返回0
            # 选择环境
            if dialog.env.currentText() == "测试":
                env = 0
            elif dialog.env.currentText() == "预发":
                env = 1
            else:
                print('环境参数错误！')
            # 暂时默认添加供应商账户
            User.add_user(self, dialog.username.text(), dialog.password.text(), '供应商', env, dialog.url.text())

            # 添加用户后清空user列表
            self.clearUsersLayout()
            self.initUsers()

    # 创建用户弹窗
    def creatUserDlg(self):
        dialog = CreatUserDlg(self)
        if dialog.exec_():  # accept 则返回1，reject返回0


            User.creat_user(self,dialog.orgname.text(),dialog.username.text(),dialog.mobile.text(),dialog.password.text(), dialog.url.text(),dialog.districtCode.text())

            # 添加用户后清空user列表
            self.clearUsersLayout()
            self.initUsers()

    # 去登陆
    def on_click(self, env, username, password, preset_url):
        # 调用ui自动化
        login(env, username, password, preset_url, self.guinie_line.text())
        print(self.guinie_line.text())

    # 编辑用户信息
    def editUser(self, user):
        # QMessageBox.question(self, "账户信息", str(user),
        #                      QMessageBox.Yes | QMessageBox.No, QMessageBox.Yes)
        dialog = EditUserDlg(self, user['username'], user['password'], user['preset_url'])
        if dialog.exec_():  # accept 则返回1，reject返回0
            # 选择环境
            if dialog.env.currentText() == "测试":
                env = 0
            elif dialog.env.currentText() == "预发":
                env = 1
            else:
                print('环境参数错误！')
            # 暂时默认修改供应商账户
            User.edit_user(self, user['username'],dialog.username.text(), dialog.password.text(), '供应商', env, dialog.url.text())
            # 添加用户后清空user列表
            self.clearUsersLayout()
            self.initUsers()

    # 公共设置QPushButton按钮css的方法
    def setButtonStyle(self, button):
        button.setStyleSheet(
            "QPushButton{color:#177ddc}"  # 字颜色
            "QPushButton{font-size:14px}"
            "QPushButton{background:rgb(45, 58, 75)}"  # 背景色
            "QPushButton:hover{background:#1f2d3d}"  # hover按键背景色
            "QPushButton{border-radius:0px}"  # 圆角半径
            # "QPushButton:pressed{background-color:#2d3a4b}"  # 按下时的样式
        )

    def setLineEditStyle(self, edit):
        edit.setStyleSheet(
            "QLineEdit{color:rgb(191, 203, 217)}"
            "QLineEdit{background:rgb(43, 52, 66)}"
            "QLineEdit{padding:5px}"
            "QLineEdit{border-radius:3px}"
            "QLineEdit{border:0px solid hsl(0deg 0% 43% / 10%)}"
        )

    def setUsertyle(self, button):
        button.setStyleSheet(
            "QPushButton{color:rgb(191, 203, 217)}"  # 字颜色
            "QPushButton{font-size:14px}"
            "QPushButton{background:rgb(45, 58, 75)}"  # 背景色
            "QPushButton:hover{background:#1f2d3d}"  # hover按键背景色
            "QPushButton{border-radius:0px}"  # 圆角半径
            # "QPushButton:pressed{background-color:#2d3a4b}"  # 按下时的样式
        )

    # 删除账户
    def delUser(self, user):
        is_del = QMessageBox.question(self, "询问", "是否删除？" + str(user['username']),
                                      QMessageBox.Yes | QMessageBox.No, QMessageBox.Yes)
        if is_del == QMessageBox.Yes:
            User().del_user(user)
            # 刷新页面
            self.clearUsersLayout()
            self.initUsers()



    # 清空 user_lay_out
    def clearUsersLayout(self):
        for i in range(self.users_v_layout.count()):
            # self.users_v_layout.itemAt(i).widget().deleteLater()
            try:
                li = self.users_v_layout.itemAt(i)
                for j in range(li.count()):
                    li.itemAt(j).widget().deleteLater()
                pass
            except Exception as e:
                continue
            finally:
                pass


if __name__ == '__main__':
    app = QApplication(sys.argv)
    demo = TabDemo()
    demo.show()
    sys.exit(app.exec_())
