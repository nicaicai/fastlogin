# -*- coding: utf-8 -*-
# @Author: yuzhou
# @Date:   2020-12-11 17:27:55
# @Last Modified by:   yuzhou
# @Last Modified time: 2021-04-29 17:26:39
import os
import sys
import json
import requests
path = os.getcwd()


class User():
    def __init__(self):
        # self.base_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        # self.base_dir = os.path.split(os.path.realpath(__file__))
        # self.file_path = self.base_dir[0] + '/user.json'
        self.file_path = path + '/user.json'

    def get_user(self, env):  # 读取测试账户信息
        with open(self.file_path, 'r', encoding='utf-8') as user_file:
            user_dict = json.load(user_file)
            if env == 0:
                user_dict = user_dict[0]["test"]
            else:
                user_dict = user_dict[0]["staging"]
            return user_dict

    def add_user(self, username, password, type, env, preset_url):  # 新增测试账户信息
        with open(self.file_path, 'r', encoding='utf-8') as user_file:
            user_dict = json.load(user_file)
            new_user = {}
            new_user['username'] = username
            new_user['password'] = password
            new_user['type'] = type
            new_user['env'] = env
            new_user['preset_url'] = preset_url
            if env == 0:
                user_dict[0]["test"].append(new_user)
            else:
                user_dict[0]["staging"].append(new_user)
        file = open(self.file_path, 'w', encoding='utf-8')
        js = json.dumps(user_dict, sort_keys=True, indent=4, separators=(',', ':'), ensure_ascii=False)
        file.write(js)
        file.close()

    def del_user(self, user, env):  # 删除用户
        if env == 0:
            env = "test"
        elif env == 1:
            env = "staging"
        else:
            print('环境参数错误')
        with open(self.file_path, 'r', encoding='utf-8') as user_file:
            user_dict = json.load(user_file)
            user_dict[0].get(env).remove(user)
        file = open(self.file_path, 'w', encoding='utf-8')
        js = json.dumps(user_dict, sort_keys=True, indent=4, separators=(',', ':'), ensure_ascii=False)
        file.write(js)
        file.close()

    def edit_user(self, username, username_edit, password_edit, type_edit, env_edit, preset_url_edit):  # 编辑测试账户信息
        with open(self.file_path, 'r', encoding='utf-8') as user_file:
            user_dict = json.load(user_file)
            if env_edit == 0:
                env = "test"
            else:
                env = "staging"
            print('当前环境：' + env)
            for index, dict in enumerate(user_dict[0].get(env)):
                if dict["username"] == username:
                    user_dict[0].get(env)[index]["username"] = username_edit
                    user_dict[0].get(env)[index]["password"] = password_edit
                    user_dict[0].get(env)[index]["type"] = type_edit
                    user_dict[0].get(env)[index]["env"] = env_edit
                    user_dict[0].get(env)[index]["preset_url"] = preset_url_edit

        #   encoding='utf-8' 不加回导致闪退
        file = open(self.file_path, 'w', encoding='utf-8')
        if user_dict == None:
            print('no')
        else:
            js = json.dumps(user_dict, sort_keys=True, indent=4, separators=(',', ':'), ensure_ascii=False)
            file.write(js)
            file.close()

    def creat_user(self, orgname, username, mobile, password, url, districtCode):
        """
        创建测试供应商（仅限test环境）
        """
        #     info = [orgname,username,"正式供应商","测试供应商",mobile,password,districtCode]
        #     importSupplier('supplier_import','supplier_import.xlsx',1,info)

        data = {
            "orgname": orgname,
            "username": username,
            "mobile": mobile,
            "password": password,
            "url": url,
            "districtCode": districtCode
        }
        r = requests.post("http://172.16.101.225:8002/creatSupplier", params=data)
        User.add_user(self, username, password, "供应商", 0, url)
        return data
