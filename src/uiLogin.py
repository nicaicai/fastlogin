# -*- coding: utf-8 -*-
# @Author: yuzhou
# @Date:   2020-12-11 16:29:39
# @Last Modified by:   wangmingjian
# @Last Modified time: 2021-11-16 13:03:41
import os
import time
import platform
from userCenter import UserCenter
from selenium import webdriver
from selenium.webdriver.common.keys import Keys


def login(env, kdt_name, username, password, preset_url, guinie_cookie):
    if platform.system().lower() == "darwin":
        # OS X
        chromedriver = "./chromedriver"
    elif platform.system().lower() == "windows":
        # win
        chromedriver = "./chromedriver.exe"
    else:
        print('暂时不支持当前环境!!!')

    option = webdriver.ChromeOptions()
    # option.add_argument("headless")
    # driver = webdriver.Chrome(chrome_options=option)

    os.environ["webdriver.chrome.driver"] = chromedriver
    driver = webdriver.Chrome(chromedriver, chrome_options=option)

    driver.maximize_window()

    login_url = 'https://account.youzan.com/login'

    if 'niming' in username:
        driver.get(preset_url)
        time.sleep(1)
        driver.find_element_by_xpath("/html/body/div[1]/div[2]/div/div/footer/div/div[2]/form/textarea").send_keys('匿名会话')
        time.sleep(1)
        driver.find_element_by_xpath('//*[@id="send-btn"]').click()
    else:
        driver.get(login_url)
        try:
            # qa环境通过验证码登陆，qa环境验证码不校验
            # //to do 如何处理预发的登陆？ 搞滑动验证码？
            driver.find_element_by_xpath(
                '//*[@id="app"]/div/div[1]/div[1]/div/div[1]/p').click()
            driver.find_element_by_xpath(
                '//*[@id="app"]/div/div[1]/div[1]/div/div[2]/span[2]').click()
            driver.find_element_by_xpath(
                '//*[@id="app"]/div/div[1]/div[1]/div/form/div[1]/div[2]/input').send_keys(username)
            driver.find_element_by_xpath(
                '//*[@id="app"]/div/div[1]/div[1]/div/form/div[2]/div[1]/input').send_keys(password)
            driver.find_element_by_xpath(
                '//*[@id="app"]/div/div[1]/div[1]/div/form/div[3]/button').click()
            # 搜索店铺并点击进入
            time.sleep(2)
            driver.find_element_by_xpath('//*[@id="js-react-container"]/div/div/div[2]/div[2]/div[1]/div[2]/input').click()
            driver.find_element_by_xpath('//*[@id="js-react-container"]/div/div/div[2]/div[2]/div[1]/div[2]/input').send_keys(kdt_name)
            driver.find_element_by_xpath('//*[@id="js-react-container"]/div/div/div[2]/div[2]/div[1]/div[2]/input').send_keys(Keys.ENTER)
            time.sleep(1)
            driver.find_element_by_xpath('//*[@id="js-react-container"]/div/div/div[2]/div[2]/div[2]/div/div[1]').click()
            time.sleep(2)
            pass
        except Exception as e:
            print('continue')
            raise e

    # 等待页面登录完成，不然会出现status=canceled
    # time.sleep(3)
    driver.get(preset_url)
