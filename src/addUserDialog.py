# -*- coding: utf-8 -*-
# @Author: yuzhou
# @Date:   2020-12-17 16:13:08
# @Last Modified by:   wangmingjian
# @Last Modified time: 2021-07-09 16:52:20
import sys
from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QLabel, QComboBox, QLineEdit, QDialogButtonBox, QGridLayout, QDialog
from user import User


class AddUserDlg(QDialog):
    '''[添加用户弹窗]
    Extends:
        QDialog
    '''

    def __init__(self, parent=None, env=None, kdt_name=None, type=None, username=None, password=None, url=None):
        super().__init__(parent)
        if env == 0:
            env = '测试'
        elif env == 1:
            env = '预发'
        # 环境选择
        envlable = QLabel("环境：")
        self.env = QComboBox()
        self.env.addItems(['测试', '预发'])
        self.env.setCurrentText(env)

        # 店铺名称
        kdtlable = QLabel("店铺名称")
        self.kdt_name = QLineEdit()
        self.kdt_name.setText(kdt_name)

        # 备注
        user_type = QLabel("备注")
        self.type = QLineEdit()
        self.type.setText(type)

        # 输入账号
        namelable = QLabel("账号:")
        self.username = QLineEdit()
        self.username.setText(username)

        # 输入密码
        passlable = QLabel("密码:")
        self.password = QLineEdit()
        self.password.setText(password)

        # 输入常用地址
        urllable = QLabel("常用测试页面:")
        self.url = QLineEdit()
        self.url.setText(url)

        # 确定取消按钮，绑定事件
        buttonBox = QDialogButtonBox()
        buttonBox.addButton("确定", QDialogButtonBox.AcceptRole)
        buttonBox.addButton("取消", QDialogButtonBox.RejectRole)
        buttonBox.accepted.connect(self.accept)
        buttonBox.rejected.connect(self.reject)

        # 创建布局
        layout = QGridLayout()
        layout.setColumnMinimumWidth(1, 500)

        # 布局环境选项
        layout.addWidget(envlable, 0, 0)
        layout.addWidget(self.env, 0, 1)

        # 账户角色布局
        layout.addWidget(user_type, 1, 0)
        layout.addWidget(self.type, 1, 1)

        # 店铺名称
        layout.addWidget(kdtlable, 2, 0)
        layout.addWidget(self.kdt_name, 2, 1)

        # 布局用户名
        layout.addWidget(namelable, 3, 0)
        layout.addWidget(self.username, 3, 1)
        # 布局密码
        layout.addWidget(passlable, 4, 0)
        layout.addWidget(self.password, 4, 1)
        # 布局常用地址链接
        layout.addWidget(urllable, 5, 0)
        layout.addWidget(self.url, 5, 1)
        # 布局确定取消按钮
        layout.addWidget(buttonBox, 6, 1)

        # 设置dialog布局
        self.setLayout(layout)
        self.setWindowTitle("添加测试用户")


class EditUserDlg(QDialog):
    '''[编辑用户弹窗]
    Extends:
        QDialog
    '''

    def __init__(self, parent=None, env=None, kdt_name=None, type=None, username=None, password=None, url=None):
        super().__init__(parent)

        if env == 0:
            env = '测试'
        elif env == 1:
            env = '预发'

        # 环境选择
        envlable = QLabel("环境：")
        self.env = QComboBox()
        self.env.addItems(['测试', '预发'])
        self.env.setCurrentText(env)

        # 店铺名称
        kdtlable = QLabel("店铺名称")
        self.kdt_name = QLineEdit()
        self.kdt_name.setText(kdt_name)

        # 备注
        user_type = QLabel("备注")
        self.type = QLineEdit()
        self.type.setText(type)

        # 输入账号
        namelable = QLabel("账号:")
        self.username = QLineEdit()
        self.username.setText(username)

        # 输入密码
        passlable = QLabel("密码:")
        self.password = QLineEdit()
        self.password.setText(password)

        # 输入常用地址
        urllable = QLabel("常用测试页面:")
        self.url = QLineEdit()
        self.url.setText(url)

        # 确定取消按钮，绑定事件
        buttonBox = QDialogButtonBox()
        buttonBox.addButton("确定", QDialogButtonBox.AcceptRole)
        buttonBox.addButton("取消", QDialogButtonBox.RejectRole)
        buttonBox.accepted.connect(self.accept)
        buttonBox.rejected.connect(self.reject)

        # 创建布局
        layout = QGridLayout()
        layout.setColumnMinimumWidth(1, 500)

        # 布局环境选项
        layout.addWidget(envlable, 0, 0)
        layout.addWidget(self.env, 0, 1)
        # 账户角色布局
        layout.addWidget(user_type, 1, 0)
        layout.addWidget(self.type, 1, 1)
        # 店铺名称
        layout.addWidget(kdtlable, 2, 0)
        layout.addWidget(self.kdt_name, 2, 1)
        # 布局用户名
        layout.addWidget(namelable, 3, 0)
        layout.addWidget(self.username, 3, 1)
        # 布局密码
        layout.addWidget(passlable, 4, 0)
        layout.addWidget(self.password, 4, 1)
        # 布局常用地址链接
        layout.addWidget(urllable, 5, 0)
        layout.addWidget(self.url, 5, 1)
        # 布局确定取消按钮
        layout.addWidget(buttonBox, 6, 1)

        # 设置dialog布局
        self.setLayout(layout)
        self.setWindowTitle("编辑测试用户")


class CreatUserDlg(QDialog):
    '''[创建用户弹窗]
    Extends:
        QDialog
    '''

    def __init__(self, parent=None, orgname=None, username=None, mobile=None, password=None, url=None):
        super().__init__(parent)

        # 输入机构名称
        orgnamelable = QLabel("机构名称:")
        self.orgname = QLineEdit()
        self.orgname.setText(orgname)

        # 输入账号
        namelable = QLabel("账号:")
        self.username = QLineEdit()
        self.username.setText(username)

        # 输入电话
        mobilelable = QLabel("电话:")
        self.mobile = QLineEdit()
        self.mobile.setText(mobile)

        # 输入密码
        passlable = QLabel("密码:")
        self.password = QLineEdit()
        self.password.setText(password)

        # 输入常用地址
        urllable = QLabel("常用测试页面:")
        self.url = QLineEdit()
        self.url.setText(url)

        # 输入区划
        districtlable = QLabel("区划代码:")
        self.districtCode = QLineEdit()
        self.districtCode.setText(url)

        # 确定取消按钮，绑定事件
        buttonBox = QDialogButtonBox()
        buttonBox.addButton("确定", QDialogButtonBox.AcceptRole)
        buttonBox.addButton("取消", QDialogButtonBox.RejectRole)
        buttonBox.accepted.connect(self.accept)
        buttonBox.rejected.connect(self.reject)

        # 创建布局
        layout = QGridLayout()
        layout.setColumnMinimumWidth(1, 500)

        # 布局机构id
        layout.addWidget(orgnamelable, 1, 0)
        layout.addWidget(self.orgname, 1, 1)
        # 布局账号
        layout.addWidget(namelable, 2, 0)
        layout.addWidget(self.username, 2, 1)
        # 布局电话
        layout.addWidget(mobilelable, 3, 0)
        layout.addWidget(self.mobile, 3, 1)
        # 布局密码
        layout.addWidget(passlable, 4, 0)
        layout.addWidget(self.password, 4, 1)
        # 布局常用地址
        layout.addWidget(urllable, 5, 0)
        layout.addWidget(self.url, 5, 1)
        # 布局区划
        layout.addWidget(districtlable, 6, 0)
        layout.addWidget(self.districtCode, 6, 1)
        # 布局确定取消按钮
        layout.addWidget(buttonBox, 7, 1)

        # 设置dialog布局
        self.setLayout(layout)
        self.setWindowTitle("创建测试用户（仅限test环境）")
