# -*- coding: utf-8 -*-
# @Author: yuzhou
# @Date:   2021-01-21 13:52:06
# @Last Modified by:   wangmingjian
# @Last Modified time: 2021-11-04 17:41:57
import sys
import subprocess
import platform
import requests
import os
import zipfile


class Chrome(object):
    """匹配驱动并下载"""

    def __init__(self):
        super(Chrome, self).__init__()

        # MAC os
        chrome_app = r"/Applications/Google\ Chrome.app/Contents/MacOS/"  # mac os chrome安装地址

        # Win
        chrome_reg = r"SOFTWARE\Google\Chrome\BLBeacon"  # win chrome注册表地址

        if platform.system().lower() == "darwin":
            # OS X
            result = subprocess.Popen([r'{}/Google\ Chrome --version'.format(chrome_app)],
                                      stdout=subprocess.PIPE, shell=True)
            # win
            self.version = [x.decode("utf-8") for x in result.stdout][0].strip().split(" ")[-1]
            self.driver_name = '/chromedriver_mac64.zip'
        elif platform.system().lower() == "windows":
            import winreg
            try:
                key = winreg.OpenKey(winreg.HKEY_CURRENT_USER, chrome_reg)
                self.version = winreg.QueryValueEx(key, "version")[0]
                self.driver_name = '/chromedriver_win32.zip'
            except Exception:
                raise Exception("查询注册表chrome版本失败!")
        else:
            print('暂时不支持当前环境')
            self.version = False

    def download_chorme_zip(self):  # 下载驱动zip
        # 整理的版本
        version_list = [
            '70.0.3538.16',
            '70.0.3538.67',
            '70.0.3538.97',
            '71.0.3578.137',
            '71.0.3578.30',
            '71.0.3578.33',
            '71.0.3578.80',
            '72.0.3626.69',
            '72.0.3626.7',
            '73.0.3683.20',
            '73.0.3683.68',
            '74.0.3729.6',
            '75.0.3770.140',
            '75.0.3770.8',
            '75.0.3770.90',
            '76.0.3809.12',
            '76.0.3809.126',
            '76.0.3809.25',
            '76.0.3809.68',
            '77.0.3865.10',
            '77.0.3865.40',
            '78.0.3904.105',
            '78.0.3904.11',
            '78.0.3904.70',
            '79.0.3945.16',
            '79.0.3945.36',
            '80.0.3987.106',
            '80.0.3987.16',
            '81.0.4044.138',
            '81.0.4044.20',
            '81.0.4044.69',
            '83.0.4103.14',
            '83.0.4103.39',
            '84.0.4147.30',
            '85.0.4183.38',
            '85.0.4183.83',
            '85.0.4183.87',
            '86.0.4240.22',
            '87.0.4280.20',
            '87.0.4280.87',
            '87.0.4280.88',
            '87.0.4280.20',
            '87.0.4280.87',
            '87.0.4280.88',
            '88.0.4324.27',
            '88.0.4324.96',
            '88.0.4324.27',
            '88.0.4324.96',
            '89.0.4389.23',
            '90.0.4430.24',
            '91.0.4472.101',
            '91.0.4472.19',
            '92.0.4515.107',
            '92.0.4515.43',
            '93.0.4577.15',
            '93.0.4577.63',
            '94.0.4606.41',
            '94.0.4606.61',
            '95.0.4638.10',
            '95.0.4638.17',
            '95.0.4638.54'
        ]
        # 88.0.4324.96
        # 88.0.4324
        if self.version == False:
            print('无法自动下载驱动，请手动下载进入source文件夹下替换')
            return '无法自动下载驱动，请手动下载进入source文件夹下替换'
        if self.version in version_list:
            driver_url = 'https://npm.taobao.org/mirrors/chromedriver/' + str(self.version) + self.driver_name
        else:
            print(self.version)
            v_list = self.version.split('.')
            v_list.pop()
            version_s = '.'.join(v_list)
            print(version_s)
            for i in version_list:
                print(i)
                if version_s in i:
                    print('当前：' + str(i))
                    driver_url = 'https://npm.taobao.org/mirrors/chromedriver/' + i + self.driver_name
                # else:
                #     return '找不到匹配的区划，请手动下载'

        # 这么匹配还是有问题的，不知道是否能兼容
        print('最新一个：')
        # print(driver_url)

        # 先判断文件是否存在
        if os.path.exists('down_driver.zip'):
            print('驱动已存在')
        else:
            # 下载文件
            # 通过url下载zip并解压
            f = requests.get(driver_url, verify=False)
            with open("down_driver.zip", "wb") as code:
                code.write(f.content)

            if platform.system().lower() == "darwin":
                # mac解压驱动
                os.system('cd {};unzip {}'.format('./', 'down_driver.zip'))
                return '驱动下载完成，请试试'
            elif platform.system().lower() == "windows":
                # win解压驱动
                with zipfile.ZipFile('down_driver.zip') as f:
                    for names in f.namelist():
                        f.extract(names, './')
            else:
                return '不支持当前环境解压'


if __name__ == '__main__':
    chrome = Chrome()
    chrome.download_chorme_zip()
