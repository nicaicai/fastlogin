# -*- coding: utf-8 -*-
# @Author: yuzhou
# @Date:   2020-12-11 16:27:06
# @Last Modified by:   wangmingjian
# @Last Modified time: 2021-07-09 16:53:45
import sys
from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QApplication, QWidget, QPushButton, QLineEdit, QLabel, QHBoxLayout, QVBoxLayout, QGridLayout, QMessageBox, QComboBox
from PyQt5.QtGui import QColor
from PyQt5 import QtGui, QtCore
from PyQt5.Qt import QInputDialog
from uiLogin import login
from user import User
from functools import partial
from addUserDialog import AddUserDlg
from addUserDialog import EditUserDlg
from addUserDialog import CreatUserDlg
from getDriver import Chrome
import requests
import time


class Button(QPushButton):
    def __init__(self, title, parent):
        super().__init__(title, parent)


class FastLogin(QWidget):
    '''[快速登陆窗口类]
    Extends:
        QWidget
    '''

    def __init__(self):
        # super().__init__()
        super(FastLogin, self).__init__()
        self.title = '快速登陆'
        self.left = 50
        self.top = 100
        self.width = 0
        self.height = 0
        self.env = 0
        self.initUI()
        # 实现自动下载驱动，能不能优化个弹窗出来？
        chrome = Chrome()
        down_result = chrome.download_chorme_zip()

    # 初始化UI
    def initUI(self):
        # 创建布局
        self.env_h_layout = QHBoxLayout()
        self.guinie_h_layout = QHBoxLayout()
        self.add_h_layout = QVBoxLayout()
        self.top_v_layout = QVBoxLayout()
        self.users_v_layout = QGridLayout()
        self.bottom_v_layout = QHBoxLayout()
        self.all_v_layout = QVBoxLayout()

        # 窗口位置和大小、标题、图标
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)

        # blur = QtWidgets.QGraphicsBlurEffect(blurRadius=5)
        # self.setGraphicsEffect(blur)

        palette = QtGui.QPalette()
        palette.setColor(self.backgroundRole(), QColor(45, 58, 75))
        self.setPalette(palette)
        self.setAutoFillBackground(True)
        self.setWindowOpacity(0.95)

        # 添加用户按钮
        self.button_add = QPushButton('添加测试用户', self)
        self.button_add.setToolTip("添加测试用户")
        self.button_add.setMinimumHeight(30)
        self.setButtonStyle(self.button_add)
        self.button_add.clicked.connect(self.addUserDlg)

        # 圭臬灰度输入框
        # self.guinie_lable = QLabel('圭臬隔离组:', self)
        # self.guinie_line = QLineEdit('guinie-isolation-63', self)
        # self.guinie_combo_box = QComboBox()
        # guinie_cookie_url = 'http://pangu.test.cai-inc.com/guinie/api/app/testList'
        # self.res = requests.get(url=guinie_cookie_url).json()
        # self.guinie_combo_box.addItem('不进隔离组')
        # try:
        #     for i in self.res['result']:
        #         self.guinie_combo_box.addItem(i['name'])
        # except Exception as e:
        #     # raise e
        #     print('圭臬接口异常')
        #     self.res = {'result': [{'name': 'guonie_error', 'value': 'guonie_error'}]}
        # pass

        # self.guinie_line.setMinimumHeight(36)
        # self.guinie_line.setAttribute(QtCore.Qt.WA_MacShowFocusRect, 0)
        # self.guinie_combo_box.setMinimumHeight(36)
        # self.guinie_combo_box.setAttribute(QtCore.Qt.WA_MacShowFocusRect, 0)
        # self.guinie_lable.setStyleSheet("QLabel{color:rgb(191, 203, 217)}")
        # self.setLineEditStyle(self.guinie_combo_box)

        # 环境标签切换按钮
        self.button_test = QPushButton('-QA-SC-', self)
        self.button_test.setToolTip("切换到测试环境用户")
        self.button_test.setMinimumHeight(30)
        self.setCurrentEnvButtonStyle(self.button_test)
        self.button_test.clicked.connect(lambda: self.button_env_change(0))
        self.button_stag = QPushButton('-预发-多人-', self)
        self.button_stag.setToolTip("切换到预发环境用户")
        self.button_stag.setMinimumHeight(30)
        self.setEnvButtonStyle(self.button_stag)
        self.button_stag.clicked.connect(lambda: self.button_env_change(1))

        # top顶部布局
        self.env_h_layout.addWidget(self.button_test)
        self.env_h_layout.addWidget(self.button_stag)
        # self.add_h_layout.addWidget(self.button_creat)
        self.add_h_layout.addWidget(self.button_add)
        self.top_v_layout.addLayout(self.add_h_layout)
        # self.guinie_h_layout.addWidget(self.guinie_lable)
        # self.guinie_h_layout.addWidget(self.guinie_combo_box)
        self.top_v_layout.addLayout(self.guinie_h_layout)
        self.top_v_layout.addLayout(self.env_h_layout)

        # 加载测试用户列表
        self.initUsers()

        # 全局布局
        self.all_v_layout.addLayout(self.top_v_layout)
        self.all_v_layout.addLayout(self.users_v_layout)
        self.all_v_layout.setContentsMargins(15, 10, 15, 10)
        # 设置这个比例，可以让top部分不延伸
        self.all_v_layout.setStretchFactor(self.top_v_layout, 1)
        self.all_v_layout.setStretchFactor(self.users_v_layout, 1)
        self.setLayout(self.all_v_layout)

    # 切换环境
    def button_env_change(self, env):
        self.env = env
        self.clearUsersLayout()
        self.initUsers()
        print(self.minimumHeight())
        # self.setGeometry(self.left, self.top, self.width, self.minimumHeight())

        # 设置一下选中的按钮颜色
        if self.env == 0:
            self.setCurrentEnvButtonStyle(self.button_test)
            self.setEnvButtonStyle(self.button_stag)
        elif self.env == 1:
            self.setCurrentEnvButtonStyle(self.button_stag)
            self.setEnvButtonStyle(self.button_test)

    # 加载users

    def initUsers(self):
        # 遍历展示测试账户按钮
        # 维护一个本地文件去获取测试账户
        user = User()
        users = user.get_user(self.env)
        for index, user in enumerate(users):
            button_username = QPushButton(
                user['kdt_name'] + ':' + user['type'], self)
            button_username.setToolTip("去快速登陆有赞")
            button_username.setMinimumHeight(36)

            # 设置按钮样式
            self.setUsertyle(button_username)
            button_username.setMinimumWidth(180)

            # 查看/编辑/删除
            button_edit = QPushButton('编辑', self)
            button_del = QPushButton('删除', self)

            self.setButtonStyle(button_edit)
            self.setButtonStyle(button_del)

            # 单个用户操作横向布局
            user_h_layout = QHBoxLayout()
            user_h_layout.addWidget(button_username)
            user_h_layout.addWidget(button_edit)
            user_h_layout.addWidget(button_del)

            # 设置对齐方式靠着顶部
            self.users_v_layout.addLayout(
                user_h_layout, index, 0, QtCore.Qt.AlignTop)

            # lambda 函数传递变量地址（无效）
            # button_username.clicked.connect(lambda: self.on_click(user['username'], user['password']))
            button_username.clicked.connect(partial(self.on_click, user['env'], user['kdt_name'],
                                                    user['username'], user['password'], user['preset_url']))
            button_edit.clicked.connect(partial(self.editUser, user))
            button_del.clicked.connect(partial(self.delUser, user))

    # 添加用户弹窗
    def addUserDlg(self):
        dialog = AddUserDlg(self, self.env)
        if dialog.exec_():  # accept 则返回1，reject返回0
            # 选择环境
            if dialog.env.currentText() == "测试":
                env = 0
            elif dialog.env.currentText() == "预发":
                env = 1
            else:
                print('环境参数错误！')
            # 暂时默认添加供应商账户
            user = User()
            user.add_user(dialog.username.text(), dialog.password.text(
            ), dialog.type.text(), env, dialog.url.text())

            # 添加用户后清空user列表
            self.clearUsersLayout()
            self.initUsers()

    # 创建用户弹窗
    def creatUserDlg(self):
        dialog = CreatUserDlg(self)
        if dialog.exec_():  # accept 则返回1，reject返回0
            user = User()
            user.creat_user(dialog.orgname.text(), dialog.username.text(), dialog.mobile.text(),
                            dialog.password.text(), dialog.url.text(), dialog.districtCode.text())

            # 添加用户后清空user列表
            self.clearUsersLayout()
            self.initUsers()

    # 去登陆
    def on_click(self, env, kdt_name, username, password, preset_url):
        # 调用ui自动化
        # name = self.guinie_combo_box.currentText()
        guinie_value = ''
        # for i in self.res['result']:
        #     if i['name'] == name:
        #         guinie_value = i['value']
        login(env, kdt_name, username, password, preset_url, guinie_value)

    # 编辑用户信息
    def editUser(self, user):
        dialog = EditUserDlg(
            self, self.env, user['kdt_name'], user['type'], user['username'], user['password'], user['preset_url'])
        if dialog.exec_():  # accept 则返回1，reject返回0
            # 暂时默认修改供应商账户
            my_user = User()
            my_user.edit_user(user['username'], dialog.username.text(),
                              dialog.password.text(), dialog.type.text(), self.env, dialog.url.text())
            # 添加用户后清空user列表
            self.clearUsersLayout()
            self.initUsers()

    # 公共设置QPushButton按钮css的方法
    def setButtonStyle(self, button):
        button.setStyleSheet(
            "QPushButton{color:#177ddc}"  # 字颜色
            "QPushButton{font-size:14px}"
            "QPushButton{background:rgb(45, 58, 75)}"  # 背景色
            "QPushButton:hover{background:#1f2d3d}"  # hover按键背景色
            "QPushButton{border-radius:0px}"  # 圆角半径
            # "QPushButton:pressed{background-color:#2d3a4b}"  # 按下时的样式
        )

    # 公共设置QPushButton按钮css的方法
    def setEnvButtonStyle(self, button):
        button.setStyleSheet(
            "QPushButton{color:#177ddc}"  # 字颜色
            "QPushButton{font-size:14px}"
            "QPushButton{background:rgb(45, 58, 75)}"  # 背景色
            "QPushButton:hover{background:#1f2d3d}"  # hover按键背景色
            "QPushButton{border-radius:0px}"  # 圆角半径
            "QPushButton:pressed{background-color:#1f2d3d}"  # 按下时的样式
        )

    def setCurrentEnvButtonStyle(self, button):
        button.setStyleSheet(
            "QPushButton{color:#177ddc}"  # 字颜色
            "QPushButton{font-size:14px}"
            "QPushButton{background:rgb(45, 58, 75)}"  # 背景色
            "QPushButton:hover{background:#1f2d3d}"  # hover按键背景色
            "QPushButton{border-radius:0px}"  # 圆角半径
            "QPushButton:active{background:#1f2d3d}"  # 按下时的样式
        )

    # 圭臬演示
    def setLineEditStyle(self, edit):
        edit.setStyleSheet(
            # 滚动条
            "QComboBox{combobox-popup: 0}"
            "QComboBox{color:rgb(191, 203, 217)}"
            "QComboBox{background:rgb(45, 58, 75)}"
        )

    def setUsertyle(self, button):
        button.setStyleSheet(
            "QPushButton{color:rgb(191, 203, 217)}"  # 字颜色
            "QPushButton{font-size:14px}"
            "QPushButton{background:rgb(45, 58, 75)}"  # 背景色
            "QPushButton:hover{background:#1f2d3d}"  # hover按键背景色
            "QPushButton{border-radius:0px}"  # 圆角半径
            # "QPushButton:pressed{background-color:#2d3a4b}"  # 按下时的样式
        )

    # 删除账户
    def delUser(self, user):
        is_del = QMessageBox.question(self, "询问", "是否删除？" + str(user['username']),
                                      QMessageBox.Yes | QMessageBox.No, QMessageBox.Yes)
        if is_del == QMessageBox.Yes:
            my_user = User()
            my_user.del_user(user, self.env)
            # 刷新页面
            self.clearUsersLayout()
            self.initUsers()

    # 清空 user_lay_out
    def clearUsersLayout(self):
        for i in range(self.users_v_layout.count()):
            # self.users_v_layout.itemAt(i).widget().deleteLater()
            try:
                li = self.users_v_layout.itemAt(i)
                for j in range(li.count()):
                    li.itemAt(j).widget().deleteLater()
                pass
            except Exception as e:
                continue
            finally:
                pass


if __name__ == '__main__':
    app = QApplication(sys.argv)
    fastLogin = FastLogin()
    fastLogin.show()
    sys.exit(app.exec_())
