# -*- coding: utf-8 -*-
# @Author: yuzhou
# @Date:   2021-01-07 17:52:33
# @Last Modified by:   yuzhou
# @Last Modified time: 2021-01-15 17:44:59
import requests
from urllib.parse import urljoin


class UserCenter(object):
    """docstring for User"""

    def __init__(self, username, password, env):
        if env == 'test':
            # test环境
            self.base_url = 'http://middle.test.zcygov.cn'
            self.base_login_url = 'http://login.test.zcygov.cn'
            print('test环境初始化：')
        elif env == 'staging':
            # staging环境
            self.base_url = 'https://middle-staging.zcygov.cn'
            self.base_login_url = 'https://login-staging.zcygov.cn'
            print('staging环境初始化：')
        elif env == 'shanghai-staging':
            # shanghai-staging环境
            self.base_url = 'http://www.staging-shanghai.cai-inc.com/'
            self.base_login_url = 'http://login.staging-shanghai.cai-inc.com/'
            print('上海staging环境初始化：')
        else:
            print('初始化参数有误！')
        url = urljoin(self.base_login_url, '/login')
        params = {
            'current_uri': urljoin(self.base_login_url, '/user-login/#/')
        }
        data = {
            'username': username,
            'password': password,
            'platformCode': 'zcy'
        }
        self.session = requests.session()
        res = self.session.post(url=url, params=params, data=data)
        self.cookies = self.session.cookies
        print('cookie:')
        print(self.session.cookies)
        # print(self.session.cookies.get('uid'))

    def get_session(self):  # 获取requests.session
        return self.session

    def get_cookies(self):  # 获取cookies
        return self.cookies

    # def user(self):
    #     url = urljoin(self.base_url, '/api/synlogin/suspendedceiling/user')
    #     res = self.session.post(url)
    #     minlog('获取用户信息：', url, res)
    #     # 这个cookie不行
    #     # print(res.cookies.get_dict())
    #     # 通过这个方法获取cookie
    #     # print(self.session.cookies.get('uid'))
    #     # print('cookie:')
    #     # print(self.session.cookies)
    #     res = res.json()
    #     important_info = {}
    #     important_info['uid'] = self.session.cookies.get('uid')
    #     important_info['org_id'] = res['orgId']
    #     important_info['operator_id'] = res['operatorId']
    #     res['important_info'] = important_info
    #     return res

