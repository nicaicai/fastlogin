##### 快速登陆工具

[TOC]

###### 启动项目：

* 拉取代码
* ` python3 fastLogin/src/fastLogin.py`

mac打包：
python3 setup.py py2app -A 本地不带包
python3 setup.py py2app 带包


###### 项目计划：

> 基础功能：

 - [x] selenium启动浏览器登陆并跳转方法

 - [x] pyqt5配置UI操作界面

 - [x] 测试用户保存到本地json文件

 - [x] 添加测试账户

 - [x] 支持test和staging环境登陆

 - [x] 展示测试账户列表：角色、账户、编辑、删除

 - [x] 更新测试账户之后如何实现刷新界面（添加/修改/删除用户后刷新）

 - [ ] 瀑布流展示账户内容

 - [ ] 圭臬隔离组选择（待虚竹提供隔离组cookie接口）

 - [x] selenium driver自动匹配下载

 - [x] 测试供应商账户的快速创建

 - [ ] 工具演示分享md文档

   

> b.优化： 

 - [ ] 账户添加时候校验账户是否可以登陆
 - [ ] 账户是否能登陆校验，并优化查看，显示更多用户信息



###### 1.背景

测试过程1中需要频繁切换登陆不同的身份，登出登陆、切换浏览器身份等操作耗，测试账户/密码有时候也记不住。



###### 2.想法

通过selenium操控浏览器自动登录，并跳转到我们常用的页面，或者打开多个常用的页面，方便我们测试。



###### 3.计划实现功能

> 1.通过pyqt5绘制UI操作界面
>
> 2.通过python+selenium实现UI自动化登录、跳转


###### 4.项目开始之前需要了解

* [Python3基础 ](https://www.runoob.com/python3/python3-tutorial.html)
* [PyQt5](http://code.py40.com/pyqt5/16.html)   (Python的一个GUI库，用于绘制界面)
* [谷歌浏览器驱动下载](http://npm.taobao.org/mirrors/chromedriver/)



###### 5.项目计划

> 一期功能：

<img src="https://sh-demo-doc.oss-cn-hangzhou.aliyuncs.com/1001WC/67ea7e0e-df24-4479-8ab9-a083662f964f.png?AWSAccessKeyId=LTAI4G5Dz4nPxVR15xKr7T4r&Expires=1611732960&Signature=TSDShvl2Inl9LJQRPo%2FpjUNehxU%3D" style="zoom:50%;" />



> json例子

```json
[
    {
        "staging":[
            {
                "env":1,
                "password":"test123456",
                "preset_url":"https://middle-staging.zcygov.cn/loan/#/admin-loan-overview",
                "type":"运营",
                "username":"admin"
            }
        ],
        "test":[
            {
                "env":0,
                "password":"test123456",
                "preset_url":"http://middle.test.zcygov.cn/loan/#/admin-loan-overview",
                "type":"运营",
                "username":"admin"
            }
        ]
    }
]
```

> 字段解释

|  字段 | 类型 | 解释 | 举例 |
|  ----  | ----  |  ----  |  ----  |
| username | String | 账户名 | admin |
| password | String | 账户对应的密码 | test123456 |
| type | String | 账户类型 | 供应商|
| env | Int | 账户环境 | 0:测试环境、1:预发环境、2:生产环境 |
| preset_url | string | 登陆后跳转到该地址 | "http://jinrong.test.zcygov.cn/luban/test/loan" |




