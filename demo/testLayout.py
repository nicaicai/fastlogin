# -*- coding: utf-8 -*-
# @Author: yuzhou
# @Date:   2021-01-08 14:12:12
# @Last Modified by:   yuzhou
# @Last Modified time: 2021-01-08 14:12:23
import sys
from PyQt5.QtWidgets import QApplication, QWidget, QLabel, QVBoxLayout, QLineEdit, QPushButton, QHBoxLayout, QVBoxLayout


class Demo(QWidget):
    def __init__(self):
        super(Demo, self).__init__()

        self.user_label = QLabel('username', self)
        self.pwd_label = QLabel('password', self)
        self.user_line = QLineEdit(self)
        self.pwd_line = QLineEdit(self)

        self.login_button = QPushButton('login', self)
        self.pwd_button = QPushButton('password', self)

        self.user_h_layout = QHBoxLayout()
        self.pwd_h_layout = QHBoxLayout()
        self.button_h_layout = QHBoxLayout()
        self.all_v_layout = QVBoxLayout()

        self.user_h_layout.addWidget(self.user_label)
        self.user_h_layout.addWidget(self.user_line)

        self.pwd_h_layout.addWidget(self.pwd_label)
        self.pwd_h_layout.addWidget(self.pwd_line)

        self.button_h_layout.addWidget(self.login_button)
        self.button_h_layout.addWidget(self.pwd_button)

        self.all_v_layout.addLayout(self.user_h_layout)
        self.all_v_layout.addLayout(self.pwd_h_layout)
        self.all_v_layout.addLayout(self.button_h_layout)

        self.setLayout(self.all_v_layout)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    demo = Demo()
    demo.show()
    sys.exit(app.exec_())
