import sys

import PyQt5.QtGui as gui

application = gui.QApplication(sys.argv)

window = gui.QScrollArea()
list = gui.QWidget()
layout = gui.QVBoxLayout(list)

for i in range(100):
    layout.addWidget(gui.QLabel(
        "A something longish, slightly convoluted example text."))

window.setWidget(list)

window.show()

sys.exit(application.exec_())
