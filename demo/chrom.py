# -*- coding: utf-8 -*-
# @Author: yuzhou
# @Date:   2021-01-20 14:50:33
# @Last Modified by:   yuzhou
# @Last Modified time: 2021-01-20 15:22:15

# 自动检查Chrome版本号
import sys
import subprocess

# MAC os
chrome_app = r"/Applications/Google\ Chrome.app/Contents/MacOS/"  # mac os chrome安装地址

# Win
chrome_reg = r"SOFTWARE\Google\Chrome\BLBeacon"  # win chrome注册表地址

env_sys = sys.platform
if platform.system().lower() == "darwin":
    # OS X
    result = subprocess.Popen([r'{}/Google\ Chrome --version'.format(chrome_app)],
                              stdout=subprocess.PIPE, shell=True)
    version = [x.decode("utf-8") for x in result.stdout][0].strip().split(" ")[-1]
elif platform.system().lower() == "windows":
    import winreg
    try:
        key = winreg.OpenKey(winreg.HKEY_CURRENT_USER, chrome_reg)
        version = winreg.QueryValueEx(key, "version")[0]  # 查询注册表chrome版本号
    except Exception:
        raise Exception("查询注册表chrome版本失败!")
else:
    print('暂时不支持当前环境')

print(version)
